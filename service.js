var express = require('express');
var bodyParser = require('body-parser');
var async = require('async');
var app = express();

var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

function checkFindElementType (type, target) {
    var element = '';

    switch ( type ) {
        case 'id':  
            element = By.id(target);
            break;
        case 'class':  
            element = By.className(target);
            break;
        case 'tag':  
            element = By.tagName(target);
            break;
        case 'name':  
            element = By.name(target);
            break;
        case 'linktext':  
            element = By.linkText(target);
            break;
        case 'partiallinktext':  
            element = By.partialLinkText(target);
            break;
        case 'css':  
            element = By.css(target);
            break;
        case 'xpath':  
            element = By.xpath(target);
            break;
    }
    return element;
}

function actionCommand(element, action, value) {
    switch ( action ) {
        case 'click':
            element.click();
        break;
        case 'submit':
            element.submit();
        break;
        case 'sendKeys':
            element.sendKeys(value);
        break;
    }
    return;
}


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.post('/', function (req, res) {
    var url = req.body.url;
    var browser = req.body.browser;
    var source = req.body.testcase;
    var count = req.body.testcase.length;
    var host = 'http://localhost:4444/wd/hub';
    var select = '';
    var chkelement = true;
    // ## Checking
    // console.log(req.body); // check post
    // console.log(req.body.testcase[3].node); // check node in testcase

    var driver = new webdriver.Builder().forBrowser(browser).usingServer('http://192.168.99.100:4444/wd/hub').build();
    
    console.log(count);

    driver.get(url);
    driver.manage().window().maximize();

    for (var i = 0; i < count; i++) {
        select = checkFindElementType(source[i].type, source[i].target);
        element = driver.findElement(select);

        // wait element load
        driver.wait(until.elementLocated(select), 10000)
              .then(null, function (err) {
                console.log('true')
        });     



        // check is displayed
        element.isDisplayed()
              .then(function (display) {
                 chkelement = display;
        });

        // element act something 
        if (chkelement) {
            actionCommand(element, source[i].action, source[i].value);
        }
        
        // element.click();

    }

    res.send('post send');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});